from datetime import datetime

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models


class CustomUserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('Пользователь должен ввести электронную почту!')
        email = self.normalize_email(email=email)
        user = self.model(email=email, **extra_fields)

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, password, **extra_fields)

    def delete_user(self, user_id):
        try:
            user = self.get(id=user_id)
            user.delete()
        except self.model.DoesNotExist:
            raise ValueError('Пользователь с указанным ID не найден')


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True, verbose_name="адресом электронной почты")
    phone = models.CharField(null=False, blank=False, unique=True, max_length=12, verbose_name='Номер телефона',
                             validators=[RegexValidator("^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$")]
                             )
    first_name = models.CharField(max_length=32, verbose_name='Имя')
    last_name = models.CharField(max_length=32, verbose_name='Фамилия')
    middle_name = models.CharField(max_length=32, verbose_name='Отчество', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    avatar = models.ImageField(default=None)
    total_grade_count = models.FloatField(default=0.0)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["first_name", 'last_name', "middle_name", 'phone', "is_staff"]

    def get_full_name(self):
        if self.middle_name is not None:
            return f"{self.last_name} {self.first_name} {self.middle_name}"
        else:
            return f"{self.last_name} {self.first_name}"

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = 'Пользователи'


class Groups(models.Model):
    title = models.CharField(default='', max_length=55)
    users = models.ManyToManyField(CustomUser, on_delete=models.CASCADE)
    password = models.CharField(max_length=55, default=None)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = 'Группы'


class TaskCategory(models.Model):
    title = models.CharField(max_length=55)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = 'Категории'


class Task(models.Model):
    user = models.ManyToManyField(CustomUser, on_delete=models.CASCADE)
    category = models.ForeignKey(TaskCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=55)
    grade = models.FloatField(default=5.0)
    grade_rating = models.FloatField(blank=True, null=True)
    is_archive = models.BooleanField(default=False)
    date_start = models.DateTimeField(default=datetime.time.now())
    date_end = models.DateTimeField(blank=True, null=True)
    price_to_sell = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = 'Задачи'


class Comment(models.Model):
    title = models.CharField(max_length=255)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = 'Комментарии'


class TaskProgress(models.Model):
    title = models.CharField(max_length=55)
    is_active = models.BooleanField(default=True)
    task = models.OneToOneField(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Прогресс выполнения задачи"
        verbose_name_plural = 'Прогресс выполнения задач'
